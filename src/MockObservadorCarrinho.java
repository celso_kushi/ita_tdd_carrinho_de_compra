import static org.junit.Assert.*;

public class MockObservadorCarrinho implements ObservadorCarrinho {

	private String nomeRecebido;
	private int valorRecebido;
	private boolean darPau = false;
	
	@Override
	public void produtoAdicionado(String nome, int valor) {
		if(darPau)
			throw new RuntimeException("Problema simulado no mock");
		nomeRecebido = nome;
		valorRecebido = valor;
	}

	public void verificaRecebimentoProduto(String nomeEsperado, int valorEsperado) {
		assertEquals(nomeRecebido, nomeEsperado);
		assertEquals(valorRecebido, valorEsperado);
	}

	public void queroQueVoceDePau() {
		darPau = true;
		
	}

}
